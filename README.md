# XMLApi POC #

#### Proof of concept of an api built on top of Slim Framework (PHP) ##

### How to start? ###

* Clone the code into a folder at your choice

    	git clone https://cnavalici@bitbucket.org/cnavalici/xmlapi.git XMLApi

* Install the requirements

	    cd XMLApi
	    php composer.phar update

* Hit the road!

	    cd public
	    php -S localhost:8000

The instructions above are for the quick built-in PHP webserver. If you're using a dedicated webserver (as Apache, Nginx) please follow their instructions in order to setup the virtual host.

*Note: All the following line are assuming that you're using the integrated webserver.*

### API Capabilities ###

In order to try the API you should use **curl** or any other specific tool. For Firefox there is a nice addon, **[HttpRequester](https://addons.mozilla.org/En-us/firefox/addon/httprequester/)**.

* Getting this README file

As a convenience, you can access this document with a GET request to root URL:

        curl -X GET http://localhost:8000/

* Posting XML requests

        curl -X POST -H 'Content-type:text/xml' --data-urlencode @ping_request.xml 'http://localhost:8000/api/'

The response will be based on the request type and the API registered services. Expect a NACK Reponse for invalid types.

    <?xml version="1.0" encoding="UTF-8"?>
    <nack>
      <header>
        <type>nack</type>
        <sender>Sender</sender>
        <recipient>DEMO</recipient>
        <reference></reference>
        <timestamp>2015-05-02T20:31:28+02:00</timestamp>
      </header>
      <body>
        <error>
          <code>404</code>
          <message>Service of type ping_requerst not implemented.</message>
        </error>
      </body>
    </nack>

### Adding a new Service ###

In order to add a new Service to accomodate new types of Request/Responses, you have to take the following steps:

**1) Add the new Service as a class extending BaseService.php**

in *lib/XMLApi/Services/*

    class FunnyService extends BaseService 

**2) Add the configuration for this new Service**

In *app/config/api_config.php* identify the part with $fileConfig and add the configuration for the new Service:

    $filesConfig = [
    ...
    'funny_request' => [
      'request_xsd' => 'funny_request',
      'response_xsd' => 'funny_response',
      'response_xml' => 'funny_response'
    ],
    ...
    ]

The array keys are mandatory and they point to the specific files (xsd or xml). The folders where these file are found are also in this configuration file (as *$xsdsLocation* and *$xmlsLocation*). Include the filenames without extensions.

Example:

    'response_xsd' => 'funny_response',

will point to:

    app/xsds/funny_response.xsd

**3) Implement the specific logic into the new class**

The specific logic goes into *getResponse* method. Don't forget about key related to config part:

     class FunnyService extends BaseService  {
        public static $serviceType = 'funny_request';
        ...
    }

### Something about testing ###

In order to run the tests:

    phpunit tests
    
For acceptance test you need a webserver running. The *tests/lib/XMLApi/APITest.php* assumes it running at *http://localhost:8000*.

Run it with coverage:

    phpunit --coverage-text tests/

### Notes ###

API build on PHP 5.5.9.
