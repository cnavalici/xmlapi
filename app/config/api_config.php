<?php

$xsdsLocation = dirname(__FILE__).'/../xsds/';
$xmlsLocation = dirname(__FILE__).'/../xmls/';

/**
 * this configuration should contain
 * key (as defined inside Services) => array(
 *      request_xsd => <filename_no_extension>,
 *      response_xsd => <filename_no_extension>,
 * )
 * files should be placed under XSDSLocation folder
 */
$filesConfig = [
    'ping_request' => [
      'request_xsd' => 'ping_request',
      'response_xsd' => 'ping_response',
      'response_xml' => 'ping_response'
    ],
    'reverse_request' => [
      'request_xsd' => 'reverse_request',
      'response_xsd' => 'reverse_response',
      'response_xml' => 'reverse_response'
    ],
    'nack' => [
        'request_xsd' => '',
        'response_xsd' => 'nack',
        'response_xml' => 'nack_response'
    ],
];
