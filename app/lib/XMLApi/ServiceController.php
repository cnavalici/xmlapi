<?php namespace XMLApi;

class ServiceController {
    private $registered = [];
    private $xsdsLocation;
    private $xsdsConfig;

    /* key names for config data */
    const REQUEST_XSD_KEY = 'request_xsd';
    const RESPONSE_XSD_KEY = 'response_xsd';
    const RESPONSE_XML_KEY = 'response_xml';

    public function __construct(array $filesConfig, $xsdsLocation, $xmlsLocation) {
        $this->xsdsConfig = $filesConfig;
        $this->xsdsLocation = $xsdsLocation;
        $this->xmlsLocation = $xmlsLocation;
    }

    /** register a service with the controller and inject some configuration data */
    public function register($service, $serviceType) {
        $config = $this->xsdsConfig[$serviceType];

        if (!in_array($serviceType, $this->registered)) {
            $service->validatorReq = $this->xsdsLocation . $config[self::REQUEST_XSD_KEY] . '.xsd';
            $service->validatorRes = $this->xsdsLocation . $config[self::RESPONSE_XSD_KEY] . '.xsd';
            $service->responseXML = $this->xmlsLocation . $config[self::RESPONSE_XML_KEY] . '.xml';

            $this->registered[$serviceType] = $service;
        }
    }

    /** gets the proper Service based on header analysis */
    public function getService($requestBody) {
        list($reqType, $reference) = $this->extractHeaderInfo($requestBody);

        if (array_key_exists($reqType, $this->registered)) {
            $service = $this->registered[$reqType];
            $service->reference = $reference;
            return $service;
        } else {
            throw new \Exception(sprintf("Service of type %s not implemented.", $reqType));
        }
    }

    /** directly retrieve Service based on type */
    public function getServiceByType($typeName) {
        if (array_key_exists($typeName, $this->registered)) {
            return $this->registered[$typeName];
        } else {
            throw new \Exception(sprintf("Service of type %s not implemented or registered yet.", $typeName));
        }
    }

    /** extracts the common header information */
    public function extractHeaderInfo($requestBody) {
        $xml = new \SimpleXMLElement($requestBody);

        $reqType = (string)$xml->header->type[0];
        $reference = (string)$xml->header->reference[0];

        if (!$reqType || !$reference) {
            throw new \Exception('Invalid header detected for incoming xml body.');
        }

        return array($reqType, $reference);
    }
}