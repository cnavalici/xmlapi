<?php namespace XMLApi\Services;

// enable user error handling
libxml_use_internal_errors(true);

abstract class BaseService {
    /* full paths to xsd and xml files */
    public $validatorReq;
    public $validatorRes;
    public $responseXML;

    /* reference coming from request */
    public $reference;

    /**
     * builds the common part of any XML Response, based on a specified template
     */
    public function buildResponse() {
        $responseDom = new \DOMDocument;
        $responseDom->load($this->responseXML);

        $responseDom->getElementsByTagName('reference')->item(0)->nodeValue = $this->reference;
        $responseDom->getElementsByTagName('timestamp')->item(0)->nodeValue = (new \DateTime())->format(\DateTime::W3C);

        return $responseDom;
    }

    /**
     * validates against a XSD file and process any error if any
     * returns the DOM if everything's fine
     */
    public function validateRequest($rawBody) {
        $dom = new \DOMDocument;
        $dom->loadXML($rawBody);

        if (!$dom->schemaValidate($this->validatorReq)) {
            // for now, no fancy error processing
            // foreach (libxml_get_errors() as $error) {
            //      var_dump($error);
            // }

            libxml_clear_errors();
            throw new \Exception("Invalid XML");
        }

        return $dom;
    }

    abstract public function getResponse($requestBody);
}