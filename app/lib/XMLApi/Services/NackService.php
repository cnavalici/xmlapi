<?php namespace XMLApi\Services;


class NackService extends BaseService {
    public static $serviceType = 'nack';

    public function getResponse($requestBody) {
        $responseDom = $this->buildResponse();

        /* specific logic here */
        /* requestBody - array[message, code] */
        $codeElem = $responseDom->getElementsByTagName('code')->item(0);
        $codeElem->nodeValue = $requestBody[1];

        $msgElem = $responseDom->getElementsByTagName('message')->item(0);
        $msgElem->nodeValue = $requestBody[0];

        return $responseDom->saveXML();
    }
}