<?php namespace XMLApi\Services;


class PingService extends BaseService {
    public static $serviceType = 'ping_request';

    public function getResponse($requestBody) {
        $requestDom = $this->validateRequest($requestBody);
        $responseDom = $this->buildResponse();

        /* specific logic here */
        $echoElem = $requestDom->getElementsByTagName('echo')->item(0);
        if ($echoElem) {
            $responseDom->getElementsByTagName('echo')->item(0)->nodeValue = $echoElem->nodeValue;
        }

        return $responseDom->saveXML();
    }
}