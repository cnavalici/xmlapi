<?php namespace XMLApi\Services;


class ReverseService extends BaseService {
    public static $serviceType = 'reverse_request';

    public function getResponse($requestBody) {
        $requestDom = $this->validateRequest($requestBody);
        $responseDom = $this->buildResponse();

        /* specific logic here */
        $echoElem = $requestDom->getElementsByTagName('string')->item(0);
        $responseDom->getElementsByTagName('string')->item(0)->nodeValue = $echoElem->nodeValue;
        $responseDom->getElementsByTagName('reverse')->item(0)->nodeValue = $this->mbReverse($echoElem->nodeValue);

        return $responseDom->saveXML();
    }

    private function mbReverse($mbString = '') {
        $len = mb_strlen($mbString);
        $reversed = '';
        while($len-- > 0) {
            $reversed .= mb_substr($mbString, $len, 1, 'UTF-8');
        }
        return $reversed;
    }
}