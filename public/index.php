<?php
require '../app/bootstrap.php';

use XMLApi\Services\PingService;
use XMLApi\Services\ReverseService;
use XMLApi\Services\NackService;
use XMLApi\ServiceController;

define('HTTP_BAD_REQUEST', 400);
define('DEBUG', true);

try {
    $controller = new ServiceController($filesConfig, $xsdsLocation, $xmlsLocation);
    $controller->register(new PingService, PingService::$serviceType);
    $controller->register(new ReverseService, ReverseService::$serviceType);
    $controller->register(new NackService, NackService::$serviceType);
} catch (\Exception $e) {
    // these errors will trigger a 500 server side, they
    // are not supposed to be user-friendly
    echo $e->getMessage();
}

$app = new \Slim\Slim(array('debug' => DEBUG));

$app->get('/', function () {
    // not required but display some README kind of file
    echo file_get_contents(dirname(__FILE__) . '/../README.html');
});


$app->post('/api/', function () use ($app, $controller) {
    $contentType = $app->request->getContentType();

    if (strpos($contentType, 'text/xml') !== FALSE) {
        try {
            $body = $app->request->getBody();
            $service = $controller->getService($body);
            echo $service->getResponse($body);
        } catch (\Exception $e) {
            $nackService = $controller->getServiceByType(NackService::$serviceType);
            echo $nackService->getResponse([$e->getMessage(), 404]);
        }
    } else {
        $app->response->setStatus(HTTP_BAD_REQUEST);
    };
});

$app->run();

// curl -X POST -H 'Content-type:text/xml' --data-urlencode @ping_request.xml 'http://localhost:8000/api/'