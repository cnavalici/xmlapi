<?php namespace XMLApi;

class APITest extends \PHPUnit_Framework_TestCase {
    const BASE_URL = 'http://localhost:8000';

    protected $client;
    protected $endpoint = '/api';

    protected function setUp()
    {
        $this->client = new \GuzzleHttp\Client([
            'base_url' => self::BASE_URL,
            'defaults' => ['exceptions' => false]
        ]);
    }

    public function testGetMethodForEndpoint() {
        $response = $this->client->get($this->endpoint);
        $this->assertEquals(404, $response->getStatusCode());
    }

    public function testGetMethodForMainPage() {
        $response = $this->client->get('/');
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testPostMethodForEndpointWithNoXML() {
        $response = $this->client->post($this->endpoint);

        # no xml will give you HTTP_BAD_REQUEST
        $this->assertEquals(400, $response->getStatusCode());
    }

    public function testPostMethodForEndpointXMLType() {
        $response = $this->client->post($this->endpoint, [
            'headers' => ['Content-Type' => 'text/xml']
        ]);

        # xml will give you positive reply
        $this->assertEquals(200, $response->getStatusCode());
    }

    public function testEndpointForSpecialNackReply() {
        # valid xml but invalid data for our API
        $data = '<?xml version="1.0" encoding="UTF-8"?></xml>';

        $response = $this->client->post($this->endpoint, [
            'headers' => ['Content-Type' => 'text/xml'],
            'body' => $data
        ]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals($response->xml()->header->type, 'nack');
    }
}
