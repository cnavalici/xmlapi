<?php namespace XMLApi;

abstract class BaseXMLApi extends \PHPUnit_Framework_TestCase {
    const SAMPLES_FOLDER = '/Samples/';

    protected function readSampleFile($filename) {
        $targetFile = dirname(__FILE__) . self::SAMPLES_FOLDER . $filename;
        return file_get_contents($targetFile);
    }

    protected function getXSDPath($filename) {
        return dirname(__FILE__) . self::SAMPLES_FOLDER . $filename . '.xsd';
    }

    protected function getXMLResponse($filename) {
        return dirname(__FILE__) . self::SAMPLES_FOLDER . $filename . '.xml';
    }
}