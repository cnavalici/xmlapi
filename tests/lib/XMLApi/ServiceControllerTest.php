<?php namespace XMLApi;

class ServiceControllerTest extends BaseXMLApi {
    public function setUp() {
        $this->filesConfig = [
            'ping_request' => [
                'request_xsd' => 'ping_request',
                'response_xsd' => 'ping_response',
                'response_xml' => 'ping_response'
            ]
        ];

        $this->controller = new ServiceController($this->filesConfig, 'xsdLocation', 'xmlLocation');
    }

    /**
     * @expectedException Exception
     */
    public function testRegisterServiceNoConfig() {
        $this->controller->register(new \stdClass(), 'dummy');
    }

    public function testRegisterNewService() {
        $service = new \XMLApi\Services\PingService();
        $this->controller->register($service, $service::$serviceType);

        $this->assertSame($this->controller->getServiceByType($service::$serviceType), $service);
    }

    /**
     * @expectedException Exception
     */
    public function testExtractEmptyHeaderInfo() {
        $requestBody = '<?xml version="1.0" encoding="UTF-8"?><ping_request><header></header><body></body></ping_request>';
        $this->controller->extractHeaderInfo($requestBody);
    }

    /**
     * @expectedException Exception
     */
    public function testExtractInvalidHeaderInfo() {
        $requestBody = 'really wrong body info';
        $this->controller->extractHeaderInfo($requestBody);
    }

    public function testExtractValidHeaderInfo() {
        $requestBody = $this->readSampleFile('ping_request.xml');
        $extracted = $this->controller->extractHeaderInfo($requestBody);

        $this->assertEquals($extracted, array('ping_request', 'ping_request_12345'));
    }

    /**
     * @expectedException Exception
     */
    public function testGetServiceByInvalidType() {
        $this->controller->getServiceByType('invalid_type');
    }

    public function testGetServiceValidType() {
        $service = new \XMLApi\Services\PingService();
        $this->controller->register($service, $service::$serviceType);

        $expectedService = $this->controller->getService($this->readSampleFile('ping_request.xml'));
        $this->assertSame($expectedService, $service);
    }

    /**
     * @expectedException Exception
     */
    public function testGetServiceNotRegistered() {
        $this->controller->getService($this->readSampleFile('ping_request.xml'));
    }
}
