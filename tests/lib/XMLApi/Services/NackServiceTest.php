<?php namespace XMLApi;

class NackServiceTest extends BaseXMLApi {
    public function setUp() {
        $this->service = new \XMLApi\Services\NackService();
        $this->service->responseXML = $this->getXMLResponse('nack_response');
    }

    public function testValidResponse() {
        $message = 'Some error message';
        $messageCode = 404;

        $response = $this->service->getResponse([$message, $messageCode]);

        $dom = new \SimpleXMLElement($response);
        $expectedMessage = (string)$dom->body->error->message[0];
        $expectedCode = (string)$dom->body->error->code[0];

        $this->assertEquals($expectedMessage, $message);
        $this->assertEquals($expectedCode, $messageCode);
    }
}
