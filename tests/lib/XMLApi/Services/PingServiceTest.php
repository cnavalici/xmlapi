<?php namespace XMLApi;

class PingServiceTest extends BaseXMLApi {
    public function setUp() {
        $this->service = new \XMLApi\Services\PingService();
        $this->service->validatorReq = $this->getXSDPath('ping_request');
        $this->service->responseXML = $this->getXMLResponse('ping_response');
    }

    /**
     * @expectedException Exception
     */
    public function testInvalidXMLRequest() {
        $invalidXML = 'string_not_xml_doh!';
        $this->service->getResponse($invalidXML);
    }

    public function testValidRequest() {
        $validXML = $this->readSampleFile('ping_request.xml');
        $response = $this->service->getResponse($validXML);

        $dom = new \SimpleXMLElement($response);
        $echoed = (string)$dom->body->echo[0];

        $this->assertEquals($echoed, 'Hello!');
    }

    public function testValidEmptyRequest() {
        $validXML = $this->readSampleFile('ping_request_empty_content.xml');
        $response = $this->service->getResponse($validXML);

        $dom = new \SimpleXMLElement($response);
        $echoed = (string)$dom->body->echo[0];

        $this->assertEmpty($echoed);
    }
}
