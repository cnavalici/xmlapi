<?php namespace XMLApi;

class ReverseServiceTest extends BaseXMLApi {
    public function setUp() {
        $this->service = new \XMLApi\Services\ReverseService();
        $this->service->validatorReq = $this->getXSDPath('reverse_request');
        $this->service->responseXML = $this->getXMLResponse('reverse_response');
    }

    /**
     * @expectedException Exception
     */
    public function testInvalidXMLRequest() {
        $invalidXML = 'string_not_xml_doh!';
        $this->service->getResponse($invalidXML);
    }

    public function testValidRequest() {
        $validXML = $this->readSampleFile('reverse_request.xml');
        $response = $this->service->getResponse($validXML);

        $dom = new \SimpleXMLElement($response);
        $string = (string)$dom->body->string[0];
        $reverse = (string)$dom->body->reverse[0];

        $this->assertEquals($string, 'Hello Şarapovă!');
        $this->assertEquals($reverse, '!ăvoparaŞ olleH');
    }

    public function testValidEmptyRequest() {
        $validXML = $this->readSampleFile('reverse_request_empty_content.xml');
        $response = $this->service->getResponse($validXML);

        $dom = new \SimpleXMLElement($response);
        $string = (string)$dom->body->string[0];
        $reverse = (string)$dom->body->reverse[0];

        $this->assertEmpty($string);
        $this->assertEmpty($reverse);
    }
}
